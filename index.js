const express = require("express");
const bodyParser = require("body-parser");
const { checkSchema, validationResult } = require("express-validator");
const rateLimit = require("express-rate-limit");
const axios = require("axios");
const cors = require("cors");

const { WeatherSchema, ApiSchema } = require("./schema");
const { SECRET_KEY, WEATHER_API } = require("./constants");

const apiLimiter = rateLimit({
  windowMs: 60 * 60 * 1000, // One hour
  max: 5,
  message: {
    errors: [
      {
        param: "api-limit",
        msg:
          "You have exceeded the allowed limit for API, please try again after an hour",
      },
    ],
  },
});

const app = express();
app.use(cors());
app.use(bodyParser.json());

// Validate all request with API
app.use(checkSchema(ApiSchema), function (req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(401).json({
      errors: errors.array().map(({ msg, param }) => ({ msg, param })),
    });
  }
  next();
});

app.get("/weather", checkSchema(WeatherSchema), apiLimiter, function (
  req,
  res
) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      errors: errors.array().map(({ msg, param }) => ({ msg, param })),
    });
  }

  const { city, country } = req.query;
  return axios
    .get(`${WEATHER_API}?q=${city},${country}&appid=${SECRET_KEY}`)
    .then((response) => {
      const { data } = response;
      return res.send({ description: data.weather[0].description });
    })
    .catch((err) => {
      const { data } = err.response;
      return res
        .status(data.cod)
        .json({ errors: [{ msg: data.message, param: "downstream" }] });
    });
});

app.listen(8080, function () {
  console.log("listening on 8080");
});
