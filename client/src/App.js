import React from "react";
import { Container, Row, Col, Form, Button, Alert } from "react-bootstrap";
import Axios from "axios";

const api = Axios.create({
  baseURL: "http://localhost:8080",
  headers: { Authorization: "Key-1" },
});

function App() {
  const [formData, setData] = React.useState({ city: "", country: "" });
  const [errors, setErrors] = React.useState({});
  const [weather, setWeather] = React.useState("");
  const [isLoading, setIsLoading] = React.useState(false);

  const otherErrors = Object.keys(errors).map((key) =>
    key !== "city" && key !== "country" ? (
      <Alert variant="danger">
        {key}: {errors[key]}
      </Alert>
    ) : null
  );

  const submitForm = (event) => {
    event.preventDefault();
    setErrors({});
    setWeather("");
    setIsLoading(true);

    api
      .get(`/weather?city=${formData.city}&country=${formData.country}`)
      .then(function (response) {
        const { data } = response;
        setWeather(
          `Current Weather in ${formData.city}, ${formData.country} is ${data.description}`
        );
        setIsLoading(false);
      })
      .catch((error) => {
        console.log(error, error.response);
        const { data: { errors = [] } = {} } = error.response || {};

        // create a key value pair from errors
        // eg: city: "Invalid city"
        setErrors(
          Object.fromEntries(
            Object.entries(errors).map(([key, { msg, param }]) => [param, msg])
          )
        );
        setIsLoading(false);
      });
  };

  const handleChange = (event) => {
    setErrors({ ...errors, [event.target.id]: null });
    setData({ ...formData, [event.target.id]: event.target.value });
  };

  return (
    <div className="App">
      <Container>
        {otherErrors}
        {weather !== "" && <Alert variant="success">{weather}</Alert>}
        <Form onSubmit={submitForm}>
          <Row>
            <Col sm={12} md={6}>
              <Form.Group as={Row} controlId="city">
                <Form.Label>Country</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter city"
                  value={formData.city}
                  onChange={handleChange}
                  isInvalid={!!errors.city}
                />

                <Form.Control.Feedback type="invalid">
                  {errors.city}
                </Form.Control.Feedback>
              </Form.Group>
            </Col>
            <Col sm={12} md={6}>
              <Form.Group as={Row} controlId="country">
                <Form.Label>City</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter country"
                  value={formData.country}
                  onChange={handleChange}
                  isInvalid={!!errors.country}
                />

                <Form.Control.Feedback type="invalid">
                  {errors.country}
                </Form.Control.Feedback>
              </Form.Group>
            </Col>
          </Row>

          <Row>
            <Col sm={12} className="center-content">
              <Button variant="primary" type="submit" disabled={isLoading}>
                {isLoading ? "Fetching.." : "Get Weather Data"}
              </Button>
            </Col>
          </Row>
        </Form>
      </Container>
    </div>
  );
}

export default App;
