module.exports = {
  ApiSchema: {
    authorization: {
      in: ["headers"],
      isIn: { options: [["Key-1", "Key-2", "Key-3", "Key-4", "Key-5"]] }, //allowed API keys
      errorMessage: "Invalid API key",
    },
  },
  WeatherSchema: {
    city: {
      in: ["query"],
      exists: true,
      isEmpty: {
        negated: true,
      },
      isString: true,
      escape: true,
      trim: true,
      errorMessage: "Invalid city",
    },
    country: {
      in: ["query"],
      exists: true,
      isEmpty: {
        negated: true,
      },
      isString: true,
      escape: true,
      trim: true,
      errorMessage: "Invalid country",
    },
  },
};
